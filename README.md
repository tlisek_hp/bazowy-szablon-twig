# Szablon Bazowy HTML

Bazowy szablon do tworzenia stron oparty o Foundation, kompatybilny z Szablonem Bazowym WP

## Instalacja

Po sklonowaniu repozytorium uruchamiamy
```shell
npm install
```

Aby rozpocząć pracę nad szablonem uruchamiamy komendę
```shell
gulp dev
```
W konsoli wyświetli się adres serwera, po wejściu i dokonaniu zmian w plikach przeglądarka odświeży się automatycznie

## Biblioteki
Szablon używa domyślnie następujących bibliotek:
- slick-carousel - do sliderów
- fancybox - z dodanym wsparciem dla natywnych galerii WP
- lazysizes - z dodanym wsparciem do obrazków dodawanych w WP

### Dodawanie nowych bibliotek

Po zainstalowaniu bibliotek przez `npm install package_name` możemy dodać wymagane pliki .js do `JSlibs` w `gulpfile.js` - zostaną one dodane do `dist/js/libs.js`, który jest załadowany automatycznie.
__NIE__ dodajemy skryptów poprzez `<script>` w plikach HTML

Jeśli biblioteka wymaga plików css, dodajemy odpowiednią ścieżkę do `pluginsToCopy` w `gulpfile.js`. Zostaną przekopiowane do `/dist/vendor`.
Później dodajemy style w `<head>`

## Wgrywanie

### Commit
Po zakończeniu pracy commitujemy na GitLab. Tam automatycznie zostanie uruchomione `gulp prod`, a pliku wystawione na GitLab Pages. 
